Dual licensed CC0 / Woodie Guthrie License at recipient's discretion.

CC0
====

https://creativecommons.org/share-your-work/public-domain/cc0/

Woodie Guthrie License
====

"This work is Copyrighted in too many places, under too many laws, for a period of too many years years, and anybody caught usin' it without our permission, will be mighty good friends of ourn, cause we don't give a dern. Publish it. Write it. Sing it. Swing to it. Yodel it. We made it, that's all we wanted to do."
